import {appConfig} from '../../../config/app'
import Vue from 'vue'

export const areaConfigureActionsModule = {
  state: {
    areaConfigureActionCollection: {}
  },
  getters: {
    areaConfigureActionCollection (state) {
      return state.areaConfigureActionCollection
    }
  },
  mutations: {
    setAreaConfigureActionCollection (state, areaConfigureActionCollection) {
      state.areaConfigureActionCollection = areaConfigureActionCollection
    }
  },
  actions: {
    fetchAreaConfigureActionCollection (context, data) {
      const url = appConfig.URL.areaConfigureAction + '/' + data.areaId + '' + (data.getParams ? data.getParams : '')

      Vue.http.get(url).then(response => {
        context.commit('setAreaConfigureActionCollection', response.data)
      })
    }
  }
}
