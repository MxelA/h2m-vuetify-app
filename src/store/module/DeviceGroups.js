import {appConfig} from '../../../config/app'
import Vue from 'vue'

export const deviceGroupsModule = {
  state: {
    deviceGroupsCollection: {}
  },
  getters: {
    deviceGroupsCollection (state) {
      return state.deviceGroupsCollection
    }
  },
  mutations: {
    setDeviceGroupsCollection (state, areaGroupsCollection) {
      state.deviceGroupsCollection = areaGroupsCollection
    }
  },
  actions: {
    fetchDeviceGroupsCollection (context, data) {
      const url = appConfig.URL.areaDeviceGroup + '/' + data.areaId + '' + (data.getParams ? data.getParams : '')

      Vue.http.get(url).then(response => {
        context.commit('setDeviceGroupsCollection', response.data)
      })
    }
  }
}
