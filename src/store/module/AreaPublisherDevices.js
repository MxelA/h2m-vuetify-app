import {appConfig} from '../../../config/app'
import Vue from 'vue'

export const areaPublisherDevicesModule = {
  state: {
    areaPublisherDeviceCollection: {}
  },
  getters: {
    areaPublisherDeviceCollection (state) {
      return state.areaPublisherDeviceCollection
    }
  },
  mutations: {
    setAreaPublisherDeviceCollection (state, areaWorkerDeviceCollection) {
      state.areaPublisherDeviceCollection = areaWorkerDeviceCollection
    },
    updateAreaDevicePublisherCollection (state, device) {
      let tempDevices = []
      for (let i = 0; i < state.areaPublisherDeviceCollection.data.length; i++) {
        if (state.areaPublisherDeviceCollection.data[i].id === device.id) {
          tempDevices.push(device)
        } else {
          tempDevices.push(state.areaPublisherDeviceCollection.data[i])
        }
      }

      state.areaPublisherDeviceCollection.data = tempDevices
      // change device data
    }
  },
  actions: {
    fetchAreaPublisherDeviceCollection (context, data) {
      const url = appConfig.URL.areaDevice + '/publisher/' + data.areaId + '' + (data.getParams ? data.getParams : '')

      Vue.http.get(url).then(response => {
        context.commit('setAreaPublisherDeviceCollection', response.data)
      })
    }
  }
}
