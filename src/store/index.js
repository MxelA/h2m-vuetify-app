/**
 * Created by Aleksandar Milic on 5/23/17.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import {appConfig} from '../../config/app'
import {devicesModule} from './module/device'
import {usersModule} from './module/Users'
import {areaModule} from './module/Area'
import {deviceGroupsModule} from './module/DeviceGroups'
import {areaWorkerDevicesModule} from './module/AreaWorkerDevices'
import {areaPublisherDevicesModule} from './module/AreaPublisherDevices'
import {areaConfigureActionsModule} from './module/AreaConfigureActions'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    devices: devicesModule,
    users: usersModule,
    area: areaModule,
    deviceGroupsModule: deviceGroupsModule,
    areaWorkerDeviceModule: areaWorkerDevicesModule,
    areaPublisherDeviceModule: areaPublisherDevicesModule,
    areaConfigureActionsModule: areaConfigureActionsModule
  },
  state: {
    alerts: {
      info: [],
      danger: [],
      success: [],
      warning: []
    },

    authUser: {
      authenticated: sessionStorage.getItem(appConfig.API_URL + 'userToken') !== null,
      id: sessionStorage.getItem(appConfig.API_URL + 'userId'),
      name: sessionStorage.getItem(appConfig.API_URL + 'userName'),
      role: sessionStorage.getItem(appConfig.API_URL + 'userRole'),
      token: sessionStorage.getItem(appConfig.API_URL + 'userToken')
    },

    progressBar: false
  },

  getters: {
    alerts (state) {
      return state.alerts
    },

    authUser (state) {
      return state.authUser
    },

    progressBar (state) {
      return state.progressBar
    }
  },

  mutations: {

    alert (state, payload) {
      var timeout

      if (payload.timeout) {
        timeout = payload.timeout
      }

      state.alerts[payload.type].push(payload.message)

      if (timeout) {
        setTimeout(() => {
          const messageForDelete = state.alerts[payload.type].find(successMessage => {
            return successMessage === payload.message
          })

          state.alerts[payload.type].splice(messageForDelete, 1)
        }, timeout)
      }
    },

    progressBar (state, status) {
      state.progressBar = status
    }
  },

  actions: {
    alert (context, payload) {
      context.commit('alert', payload)
    },

    progressBar (context, status) {
      context.commit('progressBar', status)
    },

    checkVuetifyTablePaginationAndCallAPI (context, payload) {
      let callApi = true
      for (var key in payload.newVal) {
        if (key === 'totalItems' && payload.newVal['totalItems'] && payload.newVal['totalItems'] !== payload.oldVal['totalItems']) {
          callApi = false
        }
      }

      if (callApi) {
        payload.func()
      }
    }
  }

})
