import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import UserIndex from '@/components/user/index'
import UserEdit from '@/components/user/edit'
// import UserAreas from '@/components/user/areas'
import AreaEdit from '@/components/area/edit'
import AreaInfo from '@/components/area/info'
import DeviceEdit from '@/components/device/edit'

import {store} from '../store/'

Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/users',
      name: 'userList',
      component: UserIndex
    },
    {
      path: '/user/:id?',
      name: 'userEdit',
      component: UserEdit
    },
    // {
    //   path: '/user/:id?/areas',
    //   name: 'userAreas',
    //   component: UserAreas
    // },
    {
      path: '/area/:id?/user/:userId?',
      name: 'areaEdit',
      component: AreaEdit,
      beforeEnter (to, from, next) {
        store.dispatch('fetchArea', to.params.id)
        next()
      }
    },
    {
      path: '/area/:id/info',
      name: 'areaInfo',
      component: AreaInfo
    },
    {
      path: '/area/:areaId/device/:deviceType/:deviceId?',
      name: 'deviceEdit',
      component: DeviceEdit
    }
  ]
})
